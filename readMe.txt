Note:
- Database: MySQL
	+ URL: jdbc:mysql://localhost:3306/usermanage_db?useSSL=false
	+ username: root
	+ password: 1234
- APIs:
	+ Login: http://localhost:8080/api/login (POST):
		Content-Type: application/x-www-form-urlencoded
		Body { "username": "ql1", "password": "ql1" }
	=> return token in header
	+ Find All User: http://localhost:8080/api/users (GET)
	+ Find user name by ID: http://localhost:8080/api/users/nameById/{id} (GET)
 	+ Find user startDate by ID: http://localhost:8080/api/users/startdateById/{id} (GET)
 	+ Find all information of user by ID: http://localhost:8080/api/users/allInfoById/{id} (GET)
