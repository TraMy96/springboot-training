package com.example.springboot.repository;


import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.springboot.entity.User;
import com.example.springboot.model.UserModel;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    
    @Test
    public void whenFindAll_thenReturnListUser() {
        List<User> userList = userRepository.findAll();
     
        Assert.assertNotEquals(0, userList.size());
    }
    
    @Test
    public void whenFindById_thenReturnUser() {
        UserModel user = new UserModel(1, "Nguyen Van A");  
        System.out.println("id: " + user.getId());
        User found = userRepository.findById(user.getId());
     
        Assert.assertEquals(user.getId(), found.getId());
        Assert.assertEquals(user.getName(), found.getName());
    }
    
    @Test
    public void testSaveUser_thenReturnUser() {
    	User user = new User();
        user.setName("name1");
        user.setStartDate(new Date());
        User result = userRepository.save(user);
        Assert.assertEquals(user.getName(), result.getName());
        Assert.assertEquals(user.getStartDate(), result.getStartDate());

    }

}
