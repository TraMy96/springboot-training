package com.example.springboot.restcontroller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.example.springboot.entity.User;
import com.example.springboot.model.UserModel;
import com.example.springboot.service.UserService;

import junit.framework.Assert;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

	@Autowired
    private MockMvc mvc;
 
    @MockBean
    private UserService userService;
    
    
    @Test
    public void givenUsers_whenFindAllUser_thenReturnJsonArray()
      throws Exception {
         
    	User user1 = new User();
        user1.setId(1);
        user1.setName("name1");
        user1.setStartDate(new Date());

        User user2 = new User();
        user2.setId(2);
        user2.setName("name2");
        user2.setStartDate(new Date());
        
        List<UserModel> user = new ArrayList<UserModel>();
        user.add(new UserModel(user1));
        user.add(new UserModel(user2));
     
        //given(userService.findAll()).willReturn(user);
     
        Mockito.when(userService.findAll()).thenReturn(user);
        
        mvc.perform(get("/api/users")
        		.with(user("ql1").password("ql1"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is(user1.getName())))
                .andExpect(jsonPath("$[1].name", is(user2.getName())));
    }
    
    @Test
    public void givenUser_whenFindUserNameById_thenReturnUserName()
      throws Exception {
         
    	User user1 = new User();
        user1.setId(1);
        user1.setName("name1");
        user1.setStartDate(new Date());

        UserModel user = new UserModel(user1);
        Mockito.when(userService.findById(user1.getId())).thenReturn(user);
        
             
        mvc.perform(get("/api/users/nameById/" + user1.getId())
        		.with(user("ql1").password("ql1")))
                .andExpect(status().isOk());
    }
    
    @Test
    public void givenUser_whenFindStartDateById_thenReturnUserStartdate()
      throws Exception {
         
    	User user1 = new User();
        user1.setId(1);
        user1.setName("name1");
        user1.setStartDate(new Date());

        UserModel user = new UserModel(user1);
        Mockito.when(userService.findById(user1.getId())).thenReturn(user);
        
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String dateFormat = formatter.format(user.getStartDate());
        mvc.perform(get("/api/users/startdateById/" + user1.getId())
        		.with(user("ql1").password("ql1")))
                .andExpect(status().isOk());
    }
    
    @Test
    public void givenUser_whenFindAllInfoById_thenReturnUserAllInformation()
      throws Exception {
         
    	User user1 = new User();
        user1.setId(1);
        user1.setName("name1");
        user1.setStartDate(new Date());

        UserModel user = new UserModel(user1);
        Mockito.when(userService.findById(user1.getId())).thenReturn(user);
    	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String dateFormat = formatter.format(user.getStartDate());
             
        mvc.perform(get("/api/users/allInfoById/" + user1.getId())
        		.with(user("ql1").password("ql1"))
        		.contentType(MediaType.APPLICATION_JSON))
			    .andExpect(status().isOk())
			    .andExpect(jsonPath("name", is(user.getName())))
			    .andExpect(jsonPath("startDate", is(dateFormat)));
    }
}
