package com.example.springboot.service;

import static org.mockito.Mockito.doNothing;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.springboot.entity.User;
import com.example.springboot.model.UserModel;
import com.example.springboot.repository.UserRepository;
import com.example.springboot.service.impl.UserServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

	@Autowired
	UserService userService;
	@InjectMocks
    UserServiceImpl userServiceImpl;
    @Mock
    UserRepository userRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        User user1 = new User();
        user1.setId(1);
        user1.setName("name1");
        user1.setStartDate(new Date());

        User user2 = new User();
        user2.setId(2);
        user2.setName("name2");
        user2.setStartDate(new Date());
        
        List<User> user = new ArrayList<User>();
        user.add(user1);
        user.add(user2);
        Mockito.when(userRepository.findAll()).thenReturn(user);

        Mockito.when(userRepository.findById(1)).thenReturn(user1);
        
        Mockito.when(userRepository.save(user1)).thenReturn(user1);
    }
    
    @Test
    public void testFindAll_returnListUserModel()
    {
    	List<UserModel> userList = userServiceImpl.findAll();
        
        Assert.assertNotEquals(0, userList.size());
    }
    
    @Test
    public void testFindById_returnUserModel()
    {
    	UserModel user = userServiceImpl.findById(1);
        
        Assert.assertEquals(1, user.getId(), 0);
        Assert.assertEquals("name1", user.getName());
    }
    
    @Test
    public void testCalculateLoyalPoint_thenReturnLoyalPoint()
    {
    	Date startDate = new Date();
    	long loyalPoint = userService.calculateLoyalPoint(startDate);
    	Assert.assertEquals(5, loyalPoint);
    }

}
