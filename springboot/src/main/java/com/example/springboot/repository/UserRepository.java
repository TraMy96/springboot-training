package com.example.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.springboot.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAll();
    
    User findById(Integer id);
    
    @SuppressWarnings("unchecked")
	User save(User user);
    
    void delete(User user);
}
