package com.example.springboot.dao;

import com.example.springboot.entity.Account;

public interface AccountDAO {

   
   public Account findAccount(String userName);
}