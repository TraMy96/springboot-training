package com.example.springboot.restcontroller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.entity.User;
import com.example.springboot.model.UserModel;
import com.example.springboot.service.UserService;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    UserService userService;
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<UserModel> findAll() {
    	List<UserModel> userList = userService.findAll();
        return userList;
    }
    
    @RequestMapping(value = { "/nameById/{id}" }, method = RequestMethod.GET)
    @ResponseBody
    public String findNameById(@PathVariable("id") Integer id) {
    	
    	UserModel userModel = userService.findById(id);
        return userModel.getName();
    }
    
    @RequestMapping(value = { "/startdateById/{id}" }, method = RequestMethod.GET)
    @ResponseBody
    public String findStartdateById(@PathVariable("id") Integer id) {
    	
    	UserModel userModel = userService.findById(id);
    	Date date = userModel.getStartDate();
    	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String dateFormat = formatter.format(date);
    	return dateFormat;
    }
    
    @RequestMapping(value = { "/allInfoById/{id}" }, method = RequestMethod.GET)
    @ResponseBody
    public UserModel findAllInformationById(@PathVariable("id") Integer id) {
    	
    	UserModel userModel = userService.findById(id);
        return userModel;
    }
    
}
