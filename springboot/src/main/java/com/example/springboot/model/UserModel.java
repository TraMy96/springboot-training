package com.example.springboot.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.example.springboot.entity.User;
import com.fasterxml.jackson.annotation.JsonFormat;

public class UserModel {
	private Integer id;
     
    private String name;
    
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date startDate;

    private long loyalPoint;
    
    public UserModel(User user)
    {
    	this.id = user.getId();
    	this.name = user.getName();
    	this.startDate = user.getStartDate();
    }
    
    public UserModel(Integer id, String name)
    {
    	this.id = id;
    	this.name = name;
    }
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@SuppressWarnings("deprecation")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public long getLoyalPoint() {
      
		return loyalPoint;
	}

	public void setLoyalPoint(long loyalPoint) {
		  // Định dạng thời gian


		this.loyalPoint = loyalPoint;
	}
}
