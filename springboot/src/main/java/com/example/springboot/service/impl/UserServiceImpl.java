package com.example.springboot.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springboot.entity.User;
import com.example.springboot.model.UserModel;
import com.example.springboot.repository.UserRepository;
import com.example.springboot.service.UserService;
@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
    UserRepository userRepository;
	
	@Override
	public List<UserModel> findAll()
	{
		List<UserModel> userList = new ArrayList<UserModel>();
    	for (User user: userRepository.findAll())
    	{
    		UserModel userModel = new UserModel(user);
    		userModel.setLoyalPoint(calculateLoyalPoint(userModel.getStartDate()));
    		userList.add(userModel);
    	}
        return userList;
	}
	
	@Override
	public UserModel findById( Integer id)
	{
		UserModel userModel = new UserModel(userRepository.findById(id));
		userModel.setLoyalPoint(calculateLoyalPoint(userModel.getStartDate()));
        return userModel;
	}
	

	@Override
	public long calculateLoyalPoint(Date startDate)
	{

        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();

        Date date1 = new Date();
        Date date2 = startDate;

        c1.setTime(date1);
        c2.setTime(date2);

        long noDay = (c1.getTime().getTime() - c2.getTime().getTime())
                / (24 * 3600 * 1000);

        long loyalPoint = (noDay + 1) * 5;
        return loyalPoint;
	}

}
