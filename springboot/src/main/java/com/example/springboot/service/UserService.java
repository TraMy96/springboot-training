package com.example.springboot.service;

import java.util.Date;
import java.util.List;


import com.example.springboot.entity.User;
import com.example.springboot.model.UserModel;

public interface UserService {

	public List<UserModel> findAll();
	public UserModel findById( Integer id);
	public long calculateLoyalPoint(Date startDate);
}
