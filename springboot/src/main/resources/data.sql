INSERT INTO user (name, startDate) VALUES ('Nguyen Van A', '2009-01-01');
INSERT INTO user (name, startDate) VALUES ('Le Van B', '2016-03-01');
INSERT INTO user (name, startDate) VALUES ('Tran Thi C', '2014-07-04');
INSERT INTO user (name, startDate) VALUES ('Truong Thi D', '2011-05-02');

insert into Roles
values (1, 'MOD');
insert into Roles
values (2, 'ADMIN');

insert into Accounts (User_Name, Active, Password, Role_ID, Email)
values ('nv1', 1, '$2a$10$C8ImCu2zbhnu4GiTpzPxVOppjNx7jYVfiwBm2kOlh9WnMe4uCe0wi', 1, N'123@gmail.com');

 insert into Accounts (User_Name, Active, Password, Role_ID, Email)
values ('cus1', 1, '$2a$10$htKioLhtKvEF2Xn6iEFWq.g88A/TkyAyxyoYwKIJw24yQrTmEukSm', 1, N'123@gmail.com');

insert into Accounts (User_Name, Active, Password, Role_ID, Email)
values ('ql1', 1, '$2a$10$8Cwo.DB4W7MgJA.tFMBTdeLGMnjyBFb.LrCo/DikHs6rqshHiunDq', 2,  N'abc@gmail.com');

insert into Accounts (User_Name, Active, Password, Role_ID, Email)
values ('ql2', 1, '$2a$10$bgsjWJTMg7C9BtKyLIXljuFLTqLfd8Rt01oxCZV/Z/m1Z6c/jRh8G', 2,  N'def@gmail.com');

